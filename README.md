# spinel-data

Data for OSGeoJP Workshop for the UN Vector Tile Toolkit in December 2018.

# Data details
## 6-46-30.ndjson.gz
OpenStreetMap (c) OpenStreetMap contributors

[direct link](https://osgeo-jp.gitlab.io/spinel-data/6-46-30.ndjson.gz)

## gmlk20.ndjson.gz
Global Map Sri Lanka (c) ISCGM / Survey Department, Sri Lanka

[direct link](https://osgeo-jp.gitlab.io/spinel-data/gmlk20.ndjson.gz)

